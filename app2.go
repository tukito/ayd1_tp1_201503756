package main

import (
	"net/http"
)

//Devuelve información del estudiante
func info(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.Write([]byte("Carlos Ernesto Fuentes Rasque - 201503756"))
	default:
		panic("No soportado")
	}
}

func main2() {
	//Inciso #3.1
	http.HandleFunc("/info", info)

	//Se ejecutan se levanta el listener en el puerto 4000
	http.ListenAndServe(":4000", nil)
}
