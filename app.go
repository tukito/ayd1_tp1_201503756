package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

func suma(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("templates/suma.html")
	t.Execute(w, "")
}

func resta(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("templates/resta.html")
	t.Execute(w, "")
}

func multiplica(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("templates/multiplica.html")
	t.Execute(w, "")
}

//Realiza una suma
func sumar(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		r.ParseForm()
		num1, ok1 := r.Form["a"]
		num2, ok2 := r.Form["b"]
		if ok1 && ok2 {
			a, err := strconv.Atoi(num1[0])
			if err != nil {
				panic(err.Error())
			}
			b, err := strconv.Atoi(num2[0])
			if err != nil {
				panic(err.Error())
			}
			suma := a + b
			w.Write([]byte(fmt.Sprintf("El resultado de la suma es: %[1]d.", suma)))
		}
	default: //Por defecto lo redirecciona al inicio
		panic("No soportado")
	}
}

//Realiza una resta de números enteros
func restar(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		r.ParseForm()
		num1, ok1 := r.Form["a"]
		num2, ok2 := r.Form["b"]
		if ok1 && ok2 {
			a, err := strconv.Atoi(num1[0])
			if err != nil {
				panic(err.Error())
			}
			b, err := strconv.Atoi(num2[0])
			if err != nil {
				panic(err.Error())
			}
			w.Write([]byte(fmt.Sprintf("El resultado de la resta es: %[1]d.", a-b)))
		}
	default: //Por defecto lo redirecciona al inicio
		panic("No soportado")
	}
}

//Realiza una multiplicación de números enteros
func multiplicar(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		r.ParseForm()
		num1, ok1 := r.Form["a"]
		num2, ok2 := r.Form["b"]
		if ok1 && ok2 {
			a, err := strconv.Atoi(num1[0])
			if err != nil {
				panic(err.Error())
			}
			b, err := strconv.Atoi(num2[0])
			if err != nil {
				panic(err.Error())
			}
			w.Write([]byte(fmt.Sprintf("El resultado de la multiplicación es: %[1]d.", a*b)))
		}
	default: //Por defecto lo redirecciona al inicio
		panic("No soportado")
	}
}

func main() {
	//Inciso #2.1
	http.HandleFunc("/suma", suma)
	http.HandleFunc("/sumar", sumar)
	//Inciso #4
	http.HandleFunc("/resta", resta)
	http.HandleFunc("/restar", restar)
	//Inciso #6
	http.HandleFunc("/multiplica", multiplica)
	http.HandleFunc("/multiplicar", multiplicar)

	//Se ejecutan se levanta el listener en el puerto 3000
	http.ListenAndServe(":3000", nil)
}
